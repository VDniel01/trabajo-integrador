﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntos : MonoBehaviour
{
    public int tipo;
    private int itemsCollected;
    public Text textAmountCollected;
    public Text textYouWin;


    private void SetTexts()
    {
        textAmountCollected.text = "Puntos: " + itemsCollected.ToString() + "/5";
        if (itemsCollected >= 5)
        {
            Time.timeScale = 0f;
            textYouWin.text = "Ganaste! \n Saliste de latinoamerica 🙂";
        }
    }
    void Start()
    {
        itemsCollected = 0;
        SetTexts();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            itemsCollected++;
            SetTexts();
            other.gameObject.SetActive(false);
        }
    }
}
