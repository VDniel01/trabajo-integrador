﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Texto : MonoBehaviour
{
    public GameObject msj;
    private void Start()
    {
        msj.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        msj.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        Destroy(msj);
        Destroy(this.gameObject);
    }
}