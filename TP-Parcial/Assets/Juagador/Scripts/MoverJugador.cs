﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverJugador : MonoBehaviour
{
    private Animator anima;
    public float x, y;
    public new Rigidbody rigidbody;
    public float speed = 10f;

    CharacterController cc;
    ControlCamara cam;
    public float velocidadgiro = 5;
    public float fuerzaSalto ;
    public bool puedoSaltar;

    public int tipo;

    void Start()
    {
        anima = GetComponent<Animator>();
        
        

        cc = GetComponent<CharacterController>();
        cam = FindObjectOfType<ControlCamara>();
    }
    void FixedUpdate()
    {
        Vector3 mover = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        mover = mover.x * cam.Derecha + mover.z * cam.Adelante;

        if (mover != Vector3.zero)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, cam.transform.eulerAngles.y, 0), speed);
        }
    }


    // Update is called once per frame
    void Update()
    {
        float hor = x = Input.GetAxisRaw("Horizontal");
        float ver = y = Input.GetAxisRaw("Vertical");

        if (puedoSaltar)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anima.SetBool("Salto", true);
                rigidbody.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);

            }
            anima.SetBool("TocoSuelo", true);

        }
        else
        {
            EstoyCallendo();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        }
        if (hor != 0.0f || ver != 0.0f)
        {
            Vector3 dir = transform.forward * ver + transform.right * hor;

            rigidbody.MovePosition(transform.position + dir * speed * Time.deltaTime);
        }

        anima.SetFloat("VelX", x);
        anima.SetFloat("VelY", y);

    }

    public void EstoyCallendo()
    {
        anima.SetBool("Salto", false);
        anima.SetBool("TocoSuelo", false);
    }


  

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Colleccionable") == true)
        {
            gameObject.transform.localScale = new Vector3 (1f, 1f, 1f);
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Sorua") == true)
        {
            speed += 25;
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Chiquito") == true)
        {
            gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Salt") == true)
        {
            rigidbody.AddForce(Vector3.up * 8, ForceMode.Impulse);
            
        }
        if (other.gameObject.CompareTag("vnormal") == true)
        {
            speed += 15;
            other.gameObject.SetActive(false);
        }
    }
}
