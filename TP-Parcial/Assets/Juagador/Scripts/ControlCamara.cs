﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public Transform pivote;
    public Transform pivotar;
    public float sensibilidad;
    public Vector2 input;
    public float limite = 45;

    private Vector3 derecha;

    public Vector3 Derecha { get { return derecha; } }
    private Vector3 adelante;
    public Vector3 Adelante { get { return adelante; } }

    private bool tocando;


    void Update()
    {
        derecha = transform.right.normalized;
        adelante = transform.forward.normalized;
        derecha.y = 0;
        adelante.y = 0;
        
        input.x += Input.GetAxis("Mouse X") * sensibilidad;
        input.y += Input.GetAxis("Mouse Y") * sensibilidad;
        input.y = Mathf.Clamp(input.y, -limite, limite);

        pivote.rotation = Quaternion.Euler(-input.y, input.x, 0);
        transform.position = pivotar.position;
        transform.rotation = pivotar.rotation;
    }



}

